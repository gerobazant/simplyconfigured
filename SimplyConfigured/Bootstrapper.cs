#region Legal Disclaimer
// THIS SOFTWARE IS PROVIDED UNDER THIS LICENSE ON AN �AS IS� BASIS, WITHOUT 
// WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, WITHOUT 
// LIMITATION, WARRANTIES THAT THE COVERED SOFTWARE IS FREE OF DEFECTS, 
// MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE OR NON-INFRINGING. THE ENTIRE 
// RISK AS TO THE QUALITY AND PERFORMANCE OF THE COVERED SOFTWARE IS WITH 
// YOU. SHOULD ANY OF THIS SOFTWARE PROVE DEFECTIVE IN ANY RESPECT, YOU (NOT 
// THE INITIAL DEVELOPER OR ANY OTHER CONTRIBUTOR) ASSUME THE COST OF ANY 
// NECESSARY SERVICING, REPAIR OR CORRECTION. NO USE OF THIS SOFTWARE IS 
// AUTHORIZED HEREUNDER EXCEPT UNDER THIS DISCLAIMER.
// 
// Microsoft Public License (Ms-PL) governs use of this software. If you use the software, 
// you accept this license. If you do not accept the license, do not use the software.
#endregion
using SimplyConfigured.Sources;
using SimplyConfigured.Factories;
using StructureMap;
using StructureMap.Configuration.DSL;

namespace SimplyConfigured
{
    internal class Bootstrapper
    {
        private Bootstrapper()
        {    
        }

        public static void ConfigureStructureMap()
        {
            ObjectFactory.Initialize(s => s.IncludeRegistry(new ConfigurationRegistry()));
        }
    }

    internal sealed class ConfigurationRegistry : Registry
    {
        public ConfigurationRegistry()
        {
            For<IConfigurationFactory>()
                .Use<DefaultConfigurationFactory>();

            For<DatabaseConfigurationSource>()
                .Use(() => new DatabaseConfigurationSource());

            For<IniConfigurationSource>()
                .Use(() => new IniConfigurationSource());
        }
    }
}