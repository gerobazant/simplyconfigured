#region Legal Disclaimer
// THIS SOFTWARE IS PROVIDED UNDER THIS LICENSE ON AN �AS IS� BASIS, WITHOUT 
// WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, WITHOUT 
// LIMITATION, WARRANTIES THAT THE COVERED SOFTWARE IS FREE OF DEFECTS, 
// MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE OR NON-INFRINGING. THE ENTIRE 
// RISK AS TO THE QUALITY AND PERFORMANCE OF THE COVERED SOFTWARE IS WITH 
// YOU. SHOULD ANY OF THIS SOFTWARE PROVE DEFECTIVE IN ANY RESPECT, YOU (NOT 
// THE INITIAL DEVELOPER OR ANY OTHER CONTRIBUTOR) ASSUME THE COST OF ANY 
// NECESSARY SERVICING, REPAIR OR CORRECTION. NO USE OF THIS SOFTWARE IS 
// AUTHORIZED HEREUNDER EXCEPT UNDER THIS DISCLAIMER.
// 
// Microsoft Public License (Ms-PL) governs use of this software. If you use the software, 
// you accept this license. If you do not accept the license, do not use the software.
#endregion
using System;
using System.Configuration;

namespace SimplyConfigured.Sources
{
    /// <summary>
    /// 
    /// </summary>
    public class DatabaseConfigurationSource : IConfigurationSource
    {
        public DatabaseConfigurationSource()
        {
            
        }

        private void SetDbConnection()
        {
            var connectString = ConfigurationManager.ConnectionStrings["SimplyConfigured"];
            if (connectString == null)
            {
                throw new Exception("Cannot find 'SimplyConfigured' connection string.");
            }
        }

        /// <summary>
        /// Gets the instance.
        /// </summary>
        /// <value></value>
        public IConfiguration Instance
        {
            get
            {
                Data = new SimpleConfiguration
                                {
                                    {"DatabaseConfigurationSource", "true"},
                                    {"dupvalue", "from:DatabaseConfigurationSource"}
                                };

                return Data;
            }
        }

        /// <summary>
        /// Gets the configuration data for this specific data source.
        /// </summary>
        public IConfiguration Data { private set; get; }

        private IConfiguration LoadData()
        {
            //TODO: implement this method.

            //var iniFile = new IniFileAccess(BackingFilePath);
            //var data = iniFile.GetData();
            //var config = new SimpleConfiguration();

            //foreach (var item in data)
            //{
            //    config.Add(item.Key, item.Value);
            //}

            //Data = config;
            //return Data;
            return null;
        }

        #region IConfigurationSourceObservable members

        /// <summary>
        /// Occurs when [configuration data changed].
        /// </summary>
        public event EventHandler<ConfigurationSourceChangedEventArgs> ConfigurationSourceChanged;

        /// <summary>
        /// Called when [configuration data changed].
        /// </summary>
        /// <param name="e">The e.</param>
        public void OnConfigurationSourceChanged(ConfigurationSourceChangedEventArgs e)
        {
            if (ConfigurationSourceChanged != null)
            {
                ConfigurationSourceChanged(this, e);
            }
        }

        /// <summary>
        /// Attaches the specified observer.
        /// </summary>
        /// <param name="observer">The observer.</param>
        public void Attach(IConfigurationSourceChangeObserver observer)
        {
            //TODO: implement attach method
        }

        /// <summary>
        /// Detaches the specified observer.
        /// </summary>
        /// <param name="observer">The observer.</param>
        public void Detach(IConfigurationSourceChangeObserver observer)
        {
            //TODO: implement detach method
        } 

        #endregion
    }
}