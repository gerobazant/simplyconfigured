#region Legal Disclaimer
// THIS SOFTWARE IS PROVIDED UNDER THIS LICENSE ON AN �AS IS� BASIS, WITHOUT 
// WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, WITHOUT 
// LIMITATION, WARRANTIES THAT THE COVERED SOFTWARE IS FREE OF DEFECTS, 
// MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE OR NON-INFRINGING. THE ENTIRE 
// RISK AS TO THE QUALITY AND PERFORMANCE OF THE COVERED SOFTWARE IS WITH 
// YOU. SHOULD ANY OF THIS SOFTWARE PROVE DEFECTIVE IN ANY RESPECT, YOU (NOT 
// THE INITIAL DEVELOPER OR ANY OTHER CONTRIBUTOR) ASSUME THE COST OF ANY 
// NECESSARY SERVICING, REPAIR OR CORRECTION. NO USE OF THIS SOFTWARE IS 
// AUTHORIZED HEREUNDER EXCEPT UNDER THIS DISCLAIMER.
// 
// Microsoft Public License (Ms-PL) governs use of this software. If you use the software, 
// you accept this license. If you do not accept the license, do not use the software.
#endregion
using System;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Security.Permissions;

namespace SimplyConfigured.Sources
{
    internal class FileSystemChangeObserver : IDisposable
    {
        private DateTime _lastEvent = DateTime.Now;

        /// <summary>
        /// Initializes a new instance of the <see cref="FileSystemChangeObserver"/> class.
        /// </summary>
        public FileSystemChangeObserver()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FileSystemChangeObserver"/> class.
        /// </summary>
        public FileSystemChangeObserver(Uri path)
        {
            FilePath = path;

            if (!File.Exists(path.LocalPath))
            {
                throw new ArgumentException(string.Format(CultureInfo.CurrentCulture, "File '{0}' does not exist or is not accessible.", path.LocalPath));
            }
        }

        private FileSystemWatcher Watcher { get; set; }

        public Uri FilePath { get; set; }

        /// <summary>
        /// Observes the specified file for any changes to the content.
        /// </summary>
        [SuppressMessage("Microsoft.Security", "CA2135:SecurityRuleSetLevel2MethodsShouldNotBeProtectedWithLinkDemandsFxCopRule"), EnvironmentPermission(SecurityAction.LinkDemand, Unrestricted = true)]
        public void Observe()
        {
            Observe(FilePath);
        }

        /// <summary>
        /// Observes the specified file for any changes to the content.
        /// </summary>
        /// <param name="filePath">Name of the file.</param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2135:SecurityRuleSetLevel2MethodsShouldNotBeProtectedWithLinkDemandsFxCopRule"), SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope"), EnvironmentPermission(SecurityAction.LinkDemand, Unrestricted = true)]
        public void Observe(Uri filePath)
        {
            if (filePath == null) throw new ArgumentNullException("filePath");
            
            if (!File.Exists(filePath.LocalPath))
            {
                throw new ArgumentException(
                    string.Format(
                        CultureInfo.CurrentCulture,
                        "The referenced file '{0}' does not exist. File notification could not be initialized.",
                        filePath), "filePath");
            }

            var file = Path.GetFileName(filePath.LocalPath);
            var path = Path.GetDirectoryName(filePath.LocalPath);

            Watcher = new FileSystemWatcher(path, file)
                            {
                                NotifyFilter = NotifyFilters.LastWrite
                            };

            Watcher.Changed += FileSystemFileChanged;
            Watcher.EnableRaisingEvents = true;
        }

        private void FileSystemFileChanged(object sender, FileSystemEventArgs e)
        {
            var ts = DateTime.Now - _lastEvent;

            //HACK: find a better way of suppressing the double events fired by the file system watcher.
            //  the reason for this 'hack' is that under some conditions there are more than one events
            //  that get fired if a file is saved.
            if (ts.TotalMilliseconds > 1000)
            {
                Debug.WriteLine("FileChange event captured at '{0}'", DateTime.Now.ToLongTimeString());

                OnSourceFileChanged(new ConfigurationSourceFileChangedEventArgs(e.FullPath));

                _lastEvent = DateTime.Now;
            }
        }

        #region Observer Members

        internal event EventHandler<ConfigurationSourceFileChangedEventArgs> SourceFileChanged;

        internal virtual void OnSourceFileChanged(ConfigurationSourceFileChangedEventArgs e)
        {
            if (SourceFileChanged != null)
            {
                SourceFileChanged(this, e);
            }
        }

        #endregion

        private bool _disposed = false;

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    if (Watcher != null)
                    {
                        Watcher.Dispose();
                    }
                }

                _disposed = true;
            }
        }


        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases unmanaged resources and performs other cleanup operations before the
        /// <see cref="FileSystemChangeObserver"/> is reclaimed by garbage collection.
        /// </summary>
        ~FileSystemChangeObserver()
        {
            Dispose(false);
        }
    }
}