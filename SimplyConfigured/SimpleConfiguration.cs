#region Legal Disclaimer
// THIS SOFTWARE IS PROVIDED UNDER THIS LICENSE ON AN �AS IS� BASIS, WITHOUT 
// WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, WITHOUT 
// LIMITATION, WARRANTIES THAT THE COVERED SOFTWARE IS FREE OF DEFECTS, 
// MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE OR NON-INFRINGING. THE ENTIRE 
// RISK AS TO THE QUALITY AND PERFORMANCE OF THE COVERED SOFTWARE IS WITH 
// YOU. SHOULD ANY OF THIS SOFTWARE PROVE DEFECTIVE IN ANY RESPECT, YOU (NOT 
// THE INITIAL DEVELOPER OR ANY OTHER CONTRIBUTOR) ASSUME THE COST OF ANY 
// NECESSARY SERVICING, REPAIR OR CORRECTION. NO USE OF THIS SOFTWARE IS 
// AUTHORIZED HEREUNDER EXCEPT UNDER THIS DISCLAIMER.
// 
// Microsoft Public License (Ms-PL) governs use of this software. If you use the software, 
// you accept this license. If you do not accept the license, do not use the software.
#endregion
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Runtime.Serialization;

namespace SimplyConfigured
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class SimpleConfiguration : Dictionary<string, string>, IConfiguration
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SimpleConfiguration"/> class.
        /// </summary>
        public SimpleConfiguration()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SimpleConfiguration"/> class.
        /// </summary>
        /// <param name="dictionary">The dictionary.</param>
        public SimpleConfiguration(IDictionary<string, string> dictionary) : base(dictionary)
        {}

        /// <summary>
        /// Initializes a new instance of the <see cref="SimpleConfiguration"/> class.
        /// </summary>
        /// <param name="info">The info.</param>
        /// <param name="context">The context.</param>
        protected SimpleConfiguration(SerializationInfo info, StreamingContext context) : base(info, context)
        {}

        /// <summary>
        /// Gets the value.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        public string GetValue(string key)
        {
            return base[key];

        }

        /// <summary>
        /// Gets or sets the value associated with the specified key.
        /// </summary>
        /// <returns>
        /// The value associated with the specified key. If the specified key is not found, a get operation throws a <see cref="T:System.Collections.Generic.KeyNotFoundException"/>, and a set operation creates a new element with the specified key.
        ///   </returns>
        ///   
        /// <exception cref="T:System.ArgumentNullException"><paramref name="key"/> is null.
        ///   </exception>
        ///   
        /// <exception cref="T:System.Collections.Generic.KeyNotFoundException">
        /// The property is retrieved and <paramref name="key"/> does not exist in the collection.
        ///   </exception>
        public new string this[string key]
        {
            get 
            {
                return base[key];
            }
            set
            {
                var curVal = string.Empty;
                var hasIt = false;
                if (base.ContainsKey(key))
                {
                    curVal = base[key];
                    hasIt = true;
                }
                base[key] = value;

                // a) if the value exists in the collection and is different
                // b) if the value is new
                if ((hasIt && !curVal.Equals(value)) || !hasIt)
                {
                    OnConfigurationDataChanged(new ConfigurationDataChangedEventArgs
                                                   {
                                                       Data = value
                                                   });
                }
            }
        }

        /// <summary>
        /// Gets the int.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        public int GetInt(string key)
        {
            var val = Helper.Conversion.ToDecimal(base[key]);

            if (val == null)
            {
                throw new ArgumentException(string.Format(CultureInfo.CurrentCulture, "Argument '{0}' could not be converted to int.", key));
            }

            return (int) val;
        }

        /// <summary>
        /// Gets the double.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        public double GetDouble(string key)
        {
            var val = Helper.Conversion.ToDecimal(base[key]);

            if (val == null)
            {
                throw new ArgumentException(string.Format(CultureInfo.CurrentCulture, "Argument '{0}' could not be converted to double.", key));
            }

            return (double) val;
        }

        /// <summary>
        /// Gets the decimal.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        public decimal GetDecimal(string key)
        {
            var val = Helper.Conversion.ToDecimal(base[key]);

            if (val == null)
            {
                throw new ArgumentException(string.Format(CultureInfo.CurrentCulture, "Argument '{0}' could not be converted to decimal.", key));
            }
            
            return (decimal) val;
        }

        /// <summary>
        /// Gets the list.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        public Collection<string> GetCollection(string key)
        {
            return Helper.Conversion.ToCollection(base[key]);
        }

        /// <summary>
        /// Gets the list.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="delimiters">The delimiters.</param>
        /// <returns></returns>
        public Collection<string> GetCollection(string key, params char[] delimiters)
        {
            return Helper.Conversion.ToCollection(base[key], delimiters);

        }

        /// <summary>
        /// Occurs when data in the current instance of the configuration object changes.
        /// </summary>
        public event EventHandler<ConfigurationDataChangedEventArgs> ConfigurationDataChanged;

        /// <summary>
        /// Called when data in the current instance of the configuration object changed.
        /// </summary>
        /// <param name="e">The e.</param>
        public void OnConfigurationDataChanged(ConfigurationDataChangedEventArgs e)
        {
            if (ConfigurationDataChanged != null)
            {
                ConfigurationDataChanged(this, e);
            }
        }
    }
}