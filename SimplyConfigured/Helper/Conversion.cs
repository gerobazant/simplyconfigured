﻿#region Legal Disclaimer
// THIS SOFTWARE IS PROVIDED UNDER THIS LICENSE ON AN “AS IS” BASIS, WITHOUT 
// WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, WITHOUT 
// LIMITATION, WARRANTIES THAT THE COVERED SOFTWARE IS FREE OF DEFECTS, 
// MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE OR NON-INFRINGING. THE ENTIRE 
// RISK AS TO THE QUALITY AND PERFORMANCE OF THE COVERED SOFTWARE IS WITH 
// YOU. SHOULD ANY OF THIS SOFTWARE PROVE DEFECTIVE IN ANY RESPECT, YOU (NOT 
// THE INITIAL DEVELOPER OR ANY OTHER CONTRIBUTOR) ASSUME THE COST OF ANY 
// NECESSARY SERVICING, REPAIR OR CORRECTION. NO USE OF THIS SOFTWARE IS 
// AUTHORIZED HEREUNDER EXCEPT UNDER THIS DISCLAIMER.
// 
// Microsoft Public License (Ms-PL) governs use of this software. If you use the software, 
// you accept this license. If you do not accept the license, do not use the software.
#endregion
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace SimplyConfigured.Helper
{
    /// <summary>
    /// 
    /// </summary>
    public static class MergeDictionaries
    {
        /// <summary>
        /// Merges the specified dictionary with another one.
        /// </summary>
        /// <typeparam name="TKey">The type of the key.</typeparam>
        /// <typeparam name="TValue">The type of the value.</typeparam>
        /// <param name="mergeInto">The first.</param>
        /// <param name="toMerge">The second.</param>
        public static void Merge<TKey, TValue>(this IDictionary<TKey, TValue> mergeInto, IDictionary<TKey, TValue> toMerge)
        {
            if (toMerge == null)
            {
                return;
            }

            if (mergeInto == null)
            {
                mergeInto = new Dictionary<TKey, TValue>();
            }

            foreach (var item in toMerge) 
            {
                if (mergeInto.ContainsKey(item.Key))
                {
                    mergeInto[item.Key] = item.Value;
                }
                else
                {
                    mergeInto.Add(item.Key, item.Value);
                }
            }
        }
    }

    internal static class Conversion
    {

        /// <summary>
        /// Gets the int.
        /// </summary>
        /// <param name="val">The value to be converted.</param>
        /// <returns></returns>
        public static int? ToInt(string val)
        {
            int result;
            var success = int.TryParse(val, out result);

            return success ? result : (int?)null;
        }

        /// <summary>
        /// Gets the double.
        /// </summary>
        /// <param name="val">The value to be converted.</param>
        /// <returns></returns>
        public static double? ToDouble(string val)
        {
            double result;
            var success = double.TryParse(val, out result);

            return success ? result : (double?)null;
        }

        /// <summary>
        /// Gets the decimal.
        /// </summary>
        /// <param name="val">The value to be converted.</param>
        /// <returns></returns>
        public static decimal? ToDecimal(string val)
        {
            decimal result;
            var success = decimal.TryParse(val, out result);

            return success ? result : (decimal?)null;
        }

        /// <summary>
        /// Gets the list.
        /// </summary>
        /// <param name="val">The value to be converted.</param>
        /// <returns></returns>
        public static Collection<string> ToCollection(string val)
        {
            return Conversion.ToCollection(val, ',', ';');
        }

        /// <summary>
        /// Gets the list.
        /// </summary>
        /// <param name="val">The value to be converted.</param>
        /// <param name="delimiters">The delimiters.</param>
        /// <returns></returns>
        public static Collection<string> ToCollection(string val, params char[] delimiters)
        {
            if (!string.IsNullOrEmpty(val) && delimiters != null && val.IndexOfAny(delimiters) > -1)
            {
                var col = new Collection<string>();
                var list = val.Split(delimiters).Select(p => p.Trim());

                foreach (var item in list)
                {
                    col.Add(item);
                }

                return col;
            }

            return null;
        }
    }
}
