﻿#region Legal Disclaimer
// THIS SOFTWARE IS PROVIDED UNDER THIS LICENSE ON AN “AS IS” BASIS, WITHOUT 
// WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, WITHOUT 
// LIMITATION, WARRANTIES THAT THE COVERED SOFTWARE IS FREE OF DEFECTS, 
// MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE OR NON-INFRINGING. THE ENTIRE 
// RISK AS TO THE QUALITY AND PERFORMANCE OF THE COVERED SOFTWARE IS WITH 
// YOU. SHOULD ANY OF THIS SOFTWARE PROVE DEFECTIVE IN ANY RESPECT, YOU (NOT 
// THE INITIAL DEVELOPER OR ANY OTHER CONTRIBUTOR) ASSUME THE COST OF ANY 
// NECESSARY SERVICING, REPAIR OR CORRECTION. NO USE OF THIS SOFTWARE IS 
// AUTHORIZED HEREUNDER EXCEPT UNDER THIS DISCLAIMER.
// 
// Microsoft Public License (Ms-PL) governs use of this software. If you use the software, 
// you accept this license. If you do not accept the license, do not use the software.
#endregion
using System;

namespace SimplyConfigured.Factories
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ItemChangedEventArgs<T> : EventArgs
    {
        /// <summary>
        /// Gets the index of the item the notification was created for.
        /// </summary>
        public int Index { get; private set; }

        /// <summary>
        /// Gets the item the notification was created for.
        /// </summary>
        public T Item { get; private set; }

        /// <summary>
        /// Gets the type of the change event the notification was created for.
        /// </summary>
        /// <value>
        /// The type of the changed event.
        /// </value>
        public ItemChangedEventType ChangedEventType { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="ItemChangedEventArgs{T}"/> class.
        /// </summary>
        /// <param name="index">The index of the item the notification was created for.</param>
        /// <param name="item">The item the notification was created for.</param>
        /// <param name="type">The type of the change event the notification was created for.</param>
        public ItemChangedEventArgs(int index, T item, ItemChangedEventType type)
        {
            this.Index = index;
            this.Item = item;
            this.ChangedEventType = type;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ItemChangedEventArgs{T}"/> class.
        /// </summary>
        /// <param name="item">The item the notification was created for.</param>
        /// <param name="type">The type of the change event the notification was created for.</param>
        public ItemChangedEventArgs(T item, ItemChangedEventType type)
        {
            this.Item = item;
            this.ChangedEventType = type;
        }
    }
}