#region Legal Disclaimer
// THIS SOFTWARE IS PROVIDED UNDER THIS LICENSE ON AN �AS IS� BASIS, WITHOUT 
// WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, WITHOUT 
// LIMITATION, WARRANTIES THAT THE COVERED SOFTWARE IS FREE OF DEFECTS, 
// MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE OR NON-INFRINGING. THE ENTIRE 
// RISK AS TO THE QUALITY AND PERFORMANCE OF THE COVERED SOFTWARE IS WITH 
// YOU. SHOULD ANY OF THIS SOFTWARE PROVE DEFECTIVE IN ANY RESPECT, YOU (NOT 
// THE INITIAL DEVELOPER OR ANY OTHER CONTRIBUTOR) ASSUME THE COST OF ANY 
// NECESSARY SERVICING, REPAIR OR CORRECTION. NO USE OF THIS SOFTWARE IS 
// AUTHORIZED HEREUNDER EXCEPT UNDER THIS DISCLAIMER.
// 
// Microsoft Public License (Ms-PL) governs use of this software. If you use the software, 
// you accept this license. If you do not accept the license, do not use the software.
#endregion
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using SimplyConfigured.Sources;
using SimplyConfigured.Helper;
using StructureMap;

namespace SimplyConfigured.Factories
{
    /// <summary>
    /// 
    /// </summary>
    public enum ItemChangedEventType
    {
        /// <summary>
        /// An item got added to the collection.
        /// </summary>
        ItemAdded,

        /// <summary>
        /// An item got removed from the collection.
        /// </summary>
        ItemRemoved
    }

    /// <summary>
    /// 
    /// </summary>
    public class DefaultConfigurationFactory : IConfigurationFactory
    {
        private readonly Collection<Type> _sources = new Collection<Type>();
        private readonly Type _defaultSource = typeof(DefaultConfigurationSource);

        /// <summary>
        /// Initializes a new instance of the <see cref="DefaultConfigurationFactory"/> class.
        /// </summary>
        public DefaultConfigurationFactory()
        {
            Sources.Add(_defaultSource);
        }

        /// <summary>
        /// Exposes the list of source types the factory is currently controlling.
        /// </summary>
        public Collection<Type> Sources
        {
            get { return _sources; }
        }

        /// <summary>
        /// Gets the configuration.
        /// </summary>
        /// <returns></returns>
        public IConfiguration GetConfiguration()
        {
            /* depending on the settings a default configuration will be
             * instantiated and returned.
             * TODO: Make default source configurable. 
             */
            if (_sources.Count < 1)
            {
                Sources.Add(_defaultSource);
            }

            return GetConfiguration(_sources);

        }

        /// <summary>
        /// Gets the configuration for the supplied source type.
        /// </summary>
        /// <param name="source">The source type.</param>
        /// <returns></returns>
        public IConfiguration GetConfiguration(Type source)
        {
            return GetConfiguration(source, true);
        }

        /// <summary>
        /// Gets the configuration for the supplied source type.
        /// </summary>
        /// <param name="source">The source type.</param>
        /// <param name="persistSource">if set to <c>true</c> persists the source type
        /// adds it to the <see cref="Sources"/> list.</param>
        /// <returns></returns>
        public IConfiguration GetConfiguration(Type source, bool persistSource)
        {
            var list = new Collection<Type> { source };

            return GetConfiguration(list);
        }

        /// <summary>
        /// Gets the configuration.
        /// </summary>
        /// <param name="sources"></param>
        /// <returns></returns>
        public IConfiguration GetConfiguration(Collection<Type> sources)
        {
            return GetConfiguration(sources, true);
        }


        /// <summary>
        /// Gets the configuration.
        /// </summary>
        /// <param name="sources">The sources.</param>
        /// <param name="persistSource">if set to <c>true</c> persists the source type
        /// adds it to the <see cref="Sources"/> list.</param>
        /// <returns></returns>
        public IConfiguration GetConfiguration(Collection<Type> sources, bool persistSource)
        {
            if (sources == null)
            {
                throw new ArgumentNullException("sources", "Source type parameter cannot be null.");
            }

            var locSrc = new List<Type>();
            locSrc.AddRange(sources);

            if (locSrc.Count < 1)
            {
                locSrc.Add(_defaultSource);
            }

            var config = ActivateSources(locSrc, persistSource);

            return config;
        }

        /// <summary>
        /// Activates the sources.
        /// </summary>
        /// <param name="sources">The sources.</param>
        /// <param name="persistSources">if set to <c>true</c> [persist sources].</param>
        /// <returns></returns>
        private IConfiguration ActivateSources(IEnumerable<Type> sources, bool persistSources)
        {
            if (sources == null)
            {
                throw new ArgumentNullException("sources", "Source type parameter cannot be null.");
            }

            var config = new SimpleConfiguration() as Dictionary<string, string>;

            foreach (var item in sources)
            {
                Dictionary<string, string> newConfig = ActivateSource(item, persistSources);

                config.Merge(newConfig);
            }

            return (IConfiguration)config;
        }

        /// <summary>
        /// Activates the source.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="persistSource">if set to <c>true</c> [persist source].</param>
        /// <returns></returns>
        private Dictionary<string, string> ActivateSource(Type item, bool persistSource)
        {
            var src = GetSource(item);

            if (src == null)
            {
                return new Dictionary<string, string>();
            }

            PersistSource(item, persistSource);

            var configuration = src.Instance;

            src.Attach(this);

            return configuration as Dictionary<string, string>;
        }

        /// <summary>
        /// Persists the source.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="persistSource">if set to <c>true</c> [persist source].</param>
        private void PersistSource(Type item, bool persistSource)
        {
            if (persistSource && !Sources.Contains(item))
            {
                Sources.Add(item);
            }
        }

        /// <summary>
        /// Activates the source from the specified type -- this can either be an interface 
        ///     (in which case it has to be referenced in bootstrapper.cs) or a concrete
        ///     type.
        /// </summary>
        /// <param name="item">The type that will be instantiated. This can be an interface
        ///     or concrete type.</param>
        /// <returns></returns> 
        private static IConfigurationSource GetSource(Type item)
        {
            IConfigurationSource src;

            if (item.IsInterface)
            {
                // any interfaces specied have to be referenced in Boostrapper.cs
                src = Activate(item);
            }
            else
            {
                src = Resolve(item) as IConfigurationSource;
            }

            return src;
        }

        /// <summary>
        /// Activates a single type as specified in the parameter. The type has to be an interface.
        ///     This method is coupled to our IoC container StructureMap.
        /// </summary>
        /// <param name="type">The type that needs to be activated. This cannot be a concrete type, rather
        ///     it has to be an interface specified in the Bootstrapper.cs</param>
        /// <returns></returns>
        private static IConfigurationSource Activate(Type type)
        {
            Type ioc = typeof(ObjectFactory);
            MethodInfo mi = ioc.GetMethod("Instance");

            MethodInfo miConst = mi.MakeGenericMethod(type);
            var src = (IConfigurationSource)miConst.Invoke(null, null);
            return src;
        }

        /// <summary>
        /// Resolves the specified type. The type cannot be an interface but has to
        ///     be a concrete type. Any types referenced as part of an overloaded
        ///     constructor have to be concrete types as well.
        /// </summary>
        /// <param name="type">The type that needs to be resolved. This has to be a concrete type.</param>
        /// <returns></returns>
        private static object Resolve(Type type)
        {
            var constructor = type.GetConstructors()[0];

            var constructorParameters = constructor.GetParameters();

            if (constructorParameters.Length == 0)
            {
                return Activator.CreateInstance(type);
            }

            var parameters = new List<object>(constructorParameters.Length);

            /* TODO: change the recursive call to reference a parent call to <Resolve> as well as <Activate>
             *      rather than only <Resolve> so we can accept interfaces as parameter types.
             */
            parameters.AddRange(constructorParameters.Select(parameterInfo => Resolve(parameterInfo.ParameterType)));

            return constructor.Invoke(parameters.ToArray());
        }

        #region Implementation of IConfigurationSourceChangeObserver

        /// <summary>
        /// Updates this instance.
        /// </summary>
        public void UpdateConfigurationData(object sender, ConfigurationSourceChangedEventArgs e)
        {
            //var data = e.Data as Dictionary<string, string>;

            //var config = GetConfiguration(typeof(IniConfigurationSource), false);

            //if (data != null && data.Count > 0)
            //{
            //    foreach (var item in data)
            //    {
            //        //if()
            //    }
            //}
        }

        #endregion
    }
}