#region Legal Disclaimer
// THIS SOFTWARE IS PROVIDED UNDER THIS LICENSE ON AN �AS IS� BASIS, WITHOUT 
// WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, WITHOUT 
// LIMITATION, WARRANTIES THAT THE COVERED SOFTWARE IS FREE OF DEFECTS, 
// MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE OR NON-INFRINGING. THE ENTIRE 
// RISK AS TO THE QUALITY AND PERFORMANCE OF THE COVERED SOFTWARE IS WITH 
// YOU. SHOULD ANY OF THIS SOFTWARE PROVE DEFECTIVE IN ANY RESPECT, YOU (NOT 
// THE INITIAL DEVELOPER OR ANY OTHER CONTRIBUTOR) ASSUME THE COST OF ANY 
// NECESSARY SERVICING, REPAIR OR CORRECTION. NO USE OF THIS SOFTWARE IS 
// AUTHORIZED HEREUNDER EXCEPT UNDER THIS DISCLAIMER.
// 
// Microsoft Public License (Ms-PL) governs use of this software. If you use the software, 
// you accept this license. If you do not accept the license, do not use the software.
#endregion
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;

[assembly:CLSCompliant(true)]
namespace SimplyConfigured
{
    /// <summary>
    /// This interface represents a concrete configuration implementation.
    /// </summary>
    public interface IConfiguration
    {
        /// <summary>
        /// Gets or sets the <see cref="System.String"/> with the specified key.
        /// </summary>
        string this[string key] { set; get; }

        /// <summary>
        /// Counts this instance.
        /// </summary>
        /// <returns></returns>
        int Count { get; }

        /// <summary>
        /// Gets the value.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        string GetValue(string key);

        /// <summary>
        /// Gets the int.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        int GetInt(string key);

        /// <summary>
        /// Gets the double.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        double GetDouble(string key);

        /// <summary>
        /// Gets the decimal.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        decimal GetDecimal(string key);

        /// <summary>
        /// Gets the list.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        Collection<string> GetCollection(string key);

        /// <summary>
        /// Gets the list.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="delimiters">The delimiters.</param>
        /// <returns></returns>
        Collection<string> GetCollection(string key, params char[] delimiters);

        /// <summary>
        /// Occurs when data in the current instance of the configuration object changes.
        /// </summary>
        event EventHandler<ConfigurationDataChangedEventArgs> ConfigurationDataChanged;

        /// <summary>
        /// Called when data in the current instance of the configuration object changed.
        /// </summary>
        /// <param name="e">The e.</param>
        void OnConfigurationDataChanged(ConfigurationDataChangedEventArgs e);
    }
}