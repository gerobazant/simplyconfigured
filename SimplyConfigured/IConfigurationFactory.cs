﻿#region Legal Disclaimer
// THIS SOFTWARE IS PROVIDED UNDER THIS LICENSE ON AN “AS IS” BASIS, WITHOUT 
// WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, WITHOUT 
// LIMITATION, WARRANTIES THAT THE COVERED SOFTWARE IS FREE OF DEFECTS, 
// MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE OR NON-INFRINGING. THE ENTIRE 
// RISK AS TO THE QUALITY AND PERFORMANCE OF THE COVERED SOFTWARE IS WITH 
// YOU. SHOULD ANY OF THIS SOFTWARE PROVE DEFECTIVE IN ANY RESPECT, YOU (NOT 
// THE INITIAL DEVELOPER OR ANY OTHER CONTRIBUTOR) ASSUME THE COST OF ANY 
// NECESSARY SERVICING, REPAIR OR CORRECTION. NO USE OF THIS SOFTWARE IS 
// AUTHORIZED HEREUNDER EXCEPT UNDER THIS DISCLAIMER.
// 
// Microsoft Public License (Ms-PL) governs use of this software. If you use the software, 
// you accept this license. If you do not accept the license, do not use the software.
#endregion
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace SimplyConfigured
{
    /// <summary>
    /// 
    /// </summary>
    internal interface IConfigurationFactory : IConfigurationSourceChangeObserver
    {
        /// <summary>
        /// Gets the configuration for the supplied source type.
        /// </summary>
        /// <param name="source">The source type.</param>
        /// <returns></returns>
        IConfiguration GetConfiguration(Type source);

        /// <summary>
        /// Gets the configuration for the supplied source type.
        /// </summary>
        /// <param name="source">The source type.</param>
        /// <param name="persistSource">if set to <c>true</c> persists the source type
        ///     adds it to the <see cref="Sources"/> list.
        /// </param>
        /// <returns></returns>
        IConfiguration GetConfiguration(Type source, bool persistSource);

        /// <summary>
        /// Gets the configuration.
        /// </summary>
        /// <param name="sources">The sources.</param>
        /// <returns></returns>
        IConfiguration GetConfiguration(Collection<Type> sources);

        /// <summary>
        /// Gets the configuration.
        /// </summary>
        /// <param name="sources">The sources.</param>
        /// <param name="persistSource">if set to <c>true</c> persists the source type
        ///     adds it to the <see cref="Sources"/> list.
        /// </param>
        /// <returns></returns>
        IConfiguration GetConfiguration(Collection<Type> sources, bool persistSource);

        /// <summary>
        /// Exposes the list of source types the factory is currently controlling.
        /// </summary>
        Collection<Type> Sources { get; }
    }
}